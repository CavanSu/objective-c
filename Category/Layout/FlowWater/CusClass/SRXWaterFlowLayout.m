//
//  SRXWaterFlowLayout.m
//  WaterFlow
//
//  Created by Apple on 17/5/8.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SRXWaterFlowLayout.h"

@interface SRXWaterFlowLayout()

// 存放Cell的布局属性
@property (nonatomic, strong) NSMutableArray *attributesArray;
// 存放所有列的最大Y值
@property (nonatomic, strong) NSMutableArray *eachColumnMaxYArray;
// 每行的间距
@property (nonatomic, assign) CGFloat rowMargin;
// 每列的间距
@property (nonatomic, assign) CGFloat columnMargin;
// collectionView 的内容间距
@property (nonatomic, assign) UIEdgeInsets collectionViewContentEdge;
// 列数
@property (nonatomic, assign) NSInteger columnCount;

@end

// 默认每行的间距
static const CGFloat DefaultRowMargin = 10;
// 默认每列的间距
static const CGFloat DefaultColumnMargin = 10;
// 默认collectionView 的内容间距
static const UIEdgeInsets DefaultCollectionViewContentEdge = {10, 10, 10, 10};
// 默认列数
static const NSInteger DefaultColumnCount = 3;

@implementation SRXWaterFlowLayout

#pragma mark- 布局属性数组，每列最大Y值数组 的懒加载
- (NSMutableArray *)attributesArray {
    if(!_attributesArray){
        _attributesArray = [NSMutableArray array];
    }
    return _attributesArray;
}

- (NSMutableArray *)eachCulomnMaxYArray {
    if(!_eachColumnMaxYArray){
        _eachColumnMaxYArray = [NSMutableArray array];
    }
    return _eachColumnMaxYArray;
}

#pragma mark- 默认值通过代理改变
- (CGFloat)rowMargin {
    if([self.delegate respondsToSelector:@selector(rowMarginInFlowLayout:)]){
        return [self.delegate rowMarginInFlowLayout:self];
    }
    else{
        return DefaultRowMargin;
    }
}

- (CGFloat)columnMargin {
    if([self.delegate respondsToSelector:@selector(columnMarginInFlowLayout:)]){
        return [self.delegate columnMarginInFlowLayout:self];
    }
    else{
        return DefaultColumnMargin;
    }
}

- (UIEdgeInsets)collectionViewContentEdge {
    if([self.delegate respondsToSelector:@selector(collectionViewContentEdgeInsetsInFlowLayout:)]){
        return [self.delegate collectionViewContentEdgeInsetsInFlowLayout:self];
    }
    else{
        return DefaultCollectionViewContentEdge;
    }
}

- (NSInteger)columnCount {
    if([self.delegate respondsToSelector:@selector(numberOfColumnInFlowLayout:)]){
        return [self.delegate numberOfColumnInFlowLayout:self];
    }
    else{
        return DefaultColumnCount;
    }
}

#pragma mark- 布局属性数组，每列最大Y值数组 的初始化
// 继承基类的子类， prepareLayout默认只调用一次, 当collectionView 调用reloadData 时，会再次来到此方法
- (void)prepareLayout {
    [super prepareLayout];
    
    // 当布局改变(上拉刷新)时， 清空数组
    [self.eachCulomnMaxYArray removeAllObjects];
    // 每列最大Y值的初始化
    for (int i = 0; i < self.columnCount; i++) {
        [self.eachCulomnMaxYArray addObject:@(self.collectionViewContentEdge.top)];
    }

    // 当布局改变(上拉刷新)时， 清空数组
    [self.attributesArray removeAllObjects];
    
    NSInteger amount = [self.collectionView numberOfItemsInSection:0];
    
    for (int i = 0; i < amount; i++) {
        
        NSIndexPath *path = [NSIndexPath indexPathForItem:i inSection:0];
        
        UICollectionViewLayoutAttributes *att = [self layoutAttributesForItemAtIndexPath:path];
        
        [self.attributesArray addObject:att];
    }
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    return self.attributesArray;
}

#pragma mark- 返回每个 Cell 的属性 UICollectionViewLayoutAttributes
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *att = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    // 1.宽度
    CGFloat collectionViewWidth = self.collectionView.frame.size.width;
    CGFloat w = (collectionViewWidth - self.collectionViewContentEdge.left - self.collectionViewContentEdge.right - (self.columnCount - 1) * self.columnMargin) / self.columnCount;
    
    
    // 2.找出最短的是哪一列 确定X, Y 值
    NSInteger finalRank = 0;
    CGFloat minMaxY = [self.eachCulomnMaxYArray[0] floatValue];
    
    for (int i = 1; i < self.eachCulomnMaxYArray.count; i++) {
        
        CGFloat maxY = [self.eachCulomnMaxYArray[i] floatValue];
        if(minMaxY > maxY){
            minMaxY = maxY;
            finalRank = i;
        }
    }
    
    CGFloat x = self.collectionViewContentEdge.left + finalRank * (self.columnMargin + w);
    CGFloat y = minMaxY ;
    // item当不为第一列时，y值的变化
    if(y != self.collectionViewContentEdge.top){
        y += self.rowMargin;
    }
    
    // 3.高度
    CGFloat h = [self.delegate waterFlowLayout:self itemWidth:w heightForItemAtIndexPath:indexPath];
    
    // 4.最终 attributes 的 frame
    att.frame = CGRectMake(x, y, w, h);
    
    // 5.更新最短那一列的高度
    self.eachCulomnMaxYArray[finalRank] = @(CGRectGetMaxY(att.frame));
    
    return att;
}

#pragma mark- 返回滚动的Size
- (CGSize)collectionViewContentSize {
    CGFloat eachCulomnMaxY = [self.eachCulomnMaxYArray[0] floatValue];
    
    for (int i = 1; i < self.eachCulomnMaxYArray.count; i++) {
        
        CGFloat maxY = [self.eachCulomnMaxYArray[i] floatValue];
        if(eachCulomnMaxY < maxY){
            eachCulomnMaxY = maxY;
        }
    }

    return CGSizeMake(0, eachCulomnMaxY + self.collectionViewContentEdge.bottom);
}

@end
