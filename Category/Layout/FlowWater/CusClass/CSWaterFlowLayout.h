//
//  CSWaterFlowLayout.h
//  WaterFlow
//
//  Created by CavanSu on 17/5/8.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CSWaterFlowLayout;
@protocol CSWaterFlowLayoutDelegate <NSObject>
@required
/* @brief 通过参数 itemWidth 计算， 传回每个 item 的高度 */
- (CGFloat)waterFlowLayout:(CSWaterFlowLayout *)waterFlowLayout itemWidth:(CGFloat)itemWidth heightForItemAtIndexPath:(NSIndexPath *)indexPath;
@optional

/* @brief 传回瀑布流的列数 */
- (NSInteger)numberOfColumnInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout;
/* @brief 传回每列的间距 */
- (CGFloat)columnMarginInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout;
/* @brief 传回每行的间距 */
- (CGFloat)rowMarginInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout;

/* @brief 传回 CollectionView's ContenView 的间距 */
- (UIEdgeInsets)collectionViewContentEdgeInsetsInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout;

@end

@interface CSWaterFlowLayout : UICollectionViewLayout
@property (nonatomic, weak) id<CSWaterFlowLayoutDelegate> delegate;
@end
