//
//  SRXWaterFlowLayout.h
//  WaterFlow
//
//  Created by Apple on 17/5/8.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SRXWaterFlowLayout;

@protocol SRXWaterFlowLayoutDelegate <NSObject>

@required

/* @brief 通过参数 itemWidth 计算， 传回每个 item 的高度 */
- (CGFloat)waterFlowLayout:(SRXWaterFlowLayout *)waterFlowLayout itemWidth:(CGFloat)itemWidth heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional

/* @brief 传回瀑布流的列数 */
- (NSInteger)numberOfColumnInFlowLayout:(SRXWaterFlowLayout *)waterFlowLayout;

/* @brief 传回每列的间距 */
- (CGFloat)columnMarginInFlowLayout:(SRXWaterFlowLayout *)waterFlowLayout;

/* @brief 传回每行的间距 */
- (CGFloat)rowMarginInFlowLayout:(SRXWaterFlowLayout *)waterFlowLayout;

/* @brief 传回 CollectionView's ContenView 的间距 */
- (UIEdgeInsets)collectionViewContentEdgeInsetsInFlowLayout:(SRXWaterFlowLayout *)waterFlowLayout;

@end

@interface SRXWaterFlowLayout : UICollectionViewLayout

@property (nonatomic, weak) id<SRXWaterFlowLayoutDelegate> delegate;

@end
