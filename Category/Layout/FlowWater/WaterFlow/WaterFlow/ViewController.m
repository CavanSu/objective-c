//
//  ViewController.m
//  WaterFlow
//
//  Created by CavanSu on 17/5/8.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "ViewController.h"
#import "XMGShopCell.h"
#import "CSWaterFlowLayout.h"

#import "MJRefresh.h"
#import "MJExtension.h"
#import "XMGShop.h"

@interface ViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, CSWaterFlowLayoutDelegate>
@property (nonatomic, strong) NSMutableArray *goodsArr;
@property (nonatomic, weak) UICollectionView *collecView;
@end

static NSString *cellID = @"cellID";

@implementation ViewController

- (NSMutableArray *)goodsArr {
    if(!_goodsArr){
        _goodsArr = [NSMutableArray array];
    }
    return _goodsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 加载collectionView
    [self loadCollectionView];
    // 设置refresh
    [self setUpRefresh];
}

- (void)setUpRefresh {
    self.collecView.header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewGoods)];
    [self.collecView.header beginRefreshing];
    
    self.collecView.footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreGoods)];
}

- (void)loadNewGoods {
    [self.goodsArr removeAllObjects];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *shopsArr = [XMGShop objectArrayWithFilename:@"1.plist"];
        
        [self.goodsArr addObjectsFromArray:shopsArr];
        [self.collecView reloadData];
        [self.collecView.header endRefreshing];
        
    });
}

- (void)loadMoreGoods {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSArray *shops = [XMGShop objectArrayWithFilename:@"1.plist"];
        [self.goodsArr addObjectsFromArray:shops];
        
        // 刷新数据
        [self.collecView reloadData];
        [self.collecView.footer endRefreshing];
    });
}

- (void)loadCollectionView {
    CGRect frame = self.view.bounds;
    frame.origin.y += 20;
    frame.size.height -= 20;
    // 0.创建布局
    CSWaterFlowLayout *layout = [[CSWaterFlowLayout alloc] init];
    layout.delegate = self;
    

    // 1.创建CollectionView 一定会传入布局，否则程序直接崩溃
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor whiteColor];
    collectionView.dataSource = self;
    //    collectionView.delegate = self;
    [self.view addSubview:collectionView];
    
    // 2.注册CollectionViewCell, 注册后无需创建，如果缓存池里没有，则系统直接创建
    [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([XMGShopCell class]) bundle:nil] forCellWithReuseIdentifier:cellID];
    
    self.collecView = collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.goodsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    XMGShopCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    cell.shop = self.goodsArr[indexPath.item];
    return cell;
}

#pragma mark- <CSWaterFlowLayoutDelegate>
- (CGFloat)waterFlowLayout:(CSWaterFlowLayout *)waterFlowLayout itemWidth:(CGFloat)itemWidth heightForItemAtIndexPath:(NSIndexPath *)indexPath {
    XMGShop *shop = self.goodsArr[indexPath.item];
    CGFloat h = shop.h / shop.w * itemWidth;
    return h;
}

- (NSInteger)numberOfColumnInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout {
    return 4;
}

- (CGFloat)columnMarginInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout {
    return 20;
}

- (CGFloat)rowMarginInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout {
    return 20;
}

- (UIEdgeInsets)collectionViewContentEdgeInsetsInFlowLayout:(CSWaterFlowLayout *)waterFlowLayout {
    UIEdgeInsets edge = {20, 20, 20, 20};
    return edge;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
