//
//  AppDelegate.h
//  WaterFlow
//
//  Created by CavanSu on 17/5/8.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

