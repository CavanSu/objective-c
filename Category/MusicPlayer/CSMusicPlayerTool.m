//
//  CSMusicPlayerTool.m
//
//  Created by CavanSu on 17/8/1.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import "CSMusicPlayerTool.h"
#import <AVFoundation/AVFoundation.h>

static NSMutableDictionary *musicPlayersDic;
static NSMutableDictionary *musicItemDic;
static AVPlayer *player;
static CSMusicPlayerTool *tool;
static NSString *_musicName;

#define OutputBus  0

@interface CSMusicPlayerTool ()
@property (nonatomic, assign) ExtAudioFileRef extAudioFile;
@property (nonatomic, assign) AudioUnit remoteIOUnit;
@property (nonatomic, assign) AudioStreamBasicDescription fileStreamDes;
@property (nonatomic, assign) BOOL isUnitLoop;
@property (nonatomic, copy)   NSString *unitMusicName;
@end

@implementation CSMusicPlayerTool

+ (void)initialize {
    [super initialize];
    musicPlayersDic = [NSMutableDictionary dictionary];
    musicItemDic = [NSMutableDictionary dictionary];
}
#pragma mark - <AVAudioPlayer>
// Play musicName
+ (void)audioPlayerPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop {
    
    assert(musicName);
    AVAudioPlayer *player = nil;
    player = musicPlayersDic[musicName];
    
    if (player == nil) {
        // Received a URL of a Music File
        NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:musicName withExtension:nil];
        if (fileUrl == nil) return;
        // Created a Player
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileUrl error:nil];
        // isLoop
        if (isLoop) {
            player.numberOfLoops = -1;
        }
        // Saved player to Dic
        [musicPlayersDic setObject:player forKey:musicName];
        // Prepare
        [player prepareToPlay];
    }
    
    // Play
    [player play];
}

// Pause musicName
+ (void)audioPlayerPauseMusicWithMusicName:(NSString *)musicName {
    AVAudioPlayer *player = musicPlayersDic[musicName];
    if (player) {
        [player pause];
    }
}

// Stop musicName
+ (void)audioPlayerStopMusicWithMusicName:(NSString *)musicName {
    AVAudioPlayer *player = musicPlayersDic[musicName];
    if (player) {
        [player stop];
    }
    [musicPlayersDic removeObjectForKey:musicName];
}

#pragma mark - <AVPlayer>
+ (void)playerPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop {
    
    AVPlayerItem *item = musicItemDic[musicName];
    _musicName = musicName;
    if (item == nil) {
        NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:musicName withExtension:nil];
        if (fileUrl == nil) return;
        item = [AVPlayerItem playerItemWithURL:fileUrl];
        musicItemDic[musicName] = item;
    }
    
    if (player == nil) {
        player = [[AVPlayer alloc] init];
    }
    
    if (isLoop) {
        if (tool == nil) {
            tool = [CSMusicPlayerTool sharedMusicPlayerTool];
        }
        [[NSNotificationCenter defaultCenter] addObserver:tool selector:@selector(loopPlay:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    }
    
    // Stauts is Pause, but continue to play now
    if (item == player.currentItem && player.timeControlStatus != AVPlayerTimeControlStatusPlaying) {
        [player play];
        return;
    }
    
    [player replaceCurrentItemWithPlayerItem:item];
    [player play];
}

+ (void)playerPauseMusic{
    [player pause];
}

+ (void)playerStopMusic{
    [player pause];
    [musicItemDic removeObjectForKey:_musicName];
    [player replaceCurrentItemWithPlayerItem:nil];
    
    if (tool) {
        [[NSNotificationCenter defaultCenter] removeObserver:tool];
    }
}

- (void)loopPlay:(NSNotification *)info {
    AVPlayerItem *item = [info object];
    [item seekToTime:kCMTimeZero];
    [player play];
}

#pragma mark - <OutputUnit>
+ (instancetype)sharedMusicPlayerTool {
    if (tool == nil) {
        tool = [[CSMusicPlayerTool alloc] init];
    }
    return tool;
}

+ (void)outputUnitPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop {
    
    if (tool == nil) {
        tool = [CSMusicPlayerTool sharedMusicPlayerTool];
    }
    
    if (![musicName isEqualToString:tool.unitMusicName]) {
        [tool openFileWithMusicName:musicName];
        [tool readFileProperty];
        [tool setupAudioSession];
        [tool setupOutputUnit];
    }
    
    tool.isUnitLoop = isLoop;
    [tool startUnitPlay];
}

+ (void)outputUnitPauseMusic {
    [tool pauseUnit];
}

+ (void)outputUnitStopMusic {
    [tool stopUnit];
}

- (void)openFileWithMusicName:(NSString *)musicName {
    OSStatus status ;
    NSString *urlStr = [[NSBundle mainBundle] pathForResource:musicName ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:urlStr];
    CFURLRef cfUrl = (__bridge CFURLRef) url;
    
    status = ExtAudioFileOpenURL(cfUrl, &_extAudioFile);
    [self errorWithOSStatus:status whereCall:@"ExtAudioFileOpenURL"];
    
    self.unitMusicName = musicName;
}

- (void)readFileProperty {
    OSStatus status;
    
    AudioStreamBasicDescription fileStreamDes;
    UInt32 descSize = sizeof(fileStreamDes);
    
    status = ExtAudioFileGetProperty(_extAudioFile,
                                     kExtAudioFileProperty_FileDataFormat,
                                     &descSize,
                                     &fileStreamDes);
    [self errorWithOSStatus:status whereCall:@"kExtAudioFileProperty_FileDataFormat"];
    
    UInt32 maxPktSize = 0;
    UInt32 maxPktSizeLen = sizeof(maxPktSize);
    status = ExtAudioFileGetProperty(_extAudioFile,
                                     kExtAudioFileProperty_FileMaxPacketSize,
                                     &maxPktSizeLen,
                                     &maxPktSize);
    [self errorWithOSStatus:status whereCall:@"kExtAudioFileProperty_FileMaxPacketSize"];
    
    AudioStreamBasicDescription outputDes = SignedIntLinearPCMStreamDescription();
    UInt32 sizeOutput = sizeof(outputDes);
    status = ExtAudioFileSetProperty(_extAudioFile,
                                     kExtAudioFileProperty_ClientDataFormat,
                                     sizeOutput,
                                     &outputDes);
    
    self.fileStreamDes = outputDes;
}

- (void)errorWithOSStatus:(OSStatus)status whereCall:(NSString *)message {
    if (status != noErr) {
        NSLog(@"<CSMusicPlayerTool Log> : %@ error:%d", message, (int)status);
    }
    else {
        NSLog(@"<CSMusicPlayerTool Success> : %@ Success", message);
    }
}

- (void)setupAudioSession {
    
    NSError *error = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setPreferredSampleRate:sampleRate error:&error];
    [audioSession setPreferredInputNumberOfChannels:channels error:&error];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    [audioSession setActive: YES error: nil];
}

- (void)setupOutputUnit {
    OSStatus status;
    
    AudioComponentDescription _remoteIODesc;
    _remoteIODesc.componentType = kAudioUnitType_Output;
    _remoteIODesc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    _remoteIODesc.componentManufacturer = kAudioUnitManufacturer_Apple;
    _remoteIODesc.componentFlags = 0;
    _remoteIODesc.componentFlagsMask = 0;
    
    // AudioUnit
    AudioComponent captureComponent = AudioComponentFindNext(NULL, &_remoteIODesc);
    AudioComponentInstanceNew(captureComponent, &_remoteIOUnit);
    
    // 5.打开 输出总线 bus 0 (连接喇叭)
    UInt32 one = 1;
    status = AudioUnitSetProperty(_remoteIOUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  OutputBus,
                                  &one,
                                  sizeof(one));
    [self errorWithOSStatus:status whereCall:@"Could not enable open output bus,"];
    
    // 7.设置渲染的流
    struct AudioStreamBasicDescription renderDesc = _fileStreamDes;
    status = AudioUnitSetProperty(_remoteIOUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  OutputBus,
                                  &renderDesc,
                                  sizeof(renderDesc));
    [self errorWithOSStatus:status whereCall:@"Set renderStream"];
    
    AURenderCallbackStruct renderCallback;
    renderCallback.inputProcRefCon = (__bridge void * _Nullable)(self);
    renderCallback.inputProc = captureCallBack;
    status = AudioUnitSetProperty(_remoteIOUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Input,
                                  OutputBus,
                                  &renderCallback,
                                  sizeof(renderCallback));
    [self errorWithOSStatus:status whereCall:@"Set Render Call Back"];
    
    // 8.初始化 AudioUnit
    AudioUnitInitialize(_remoteIOUnit);
}

- (void)startUnitPlay {
    AudioOutputUnitStart(_remoteIOUnit);
}

- (void)pauseUnit {
    AudioOutputUnitStop(_remoteIOUnit);
}

- (void)stopUnit {
    AudioOutputUnitStop(_remoteIOUnit);
    ExtAudioFileSeek(_extAudioFile, 0);
}

AudioStreamBasicDescription SignedIntLinearPCMStreamDescription() {
    
    AudioStreamBasicDescription destFormat;
    
    destFormat.mFormatID = kAudioFormatLinearPCM;
    destFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    destFormat.mBitsPerChannel = 16;
    destFormat.mChannelsPerFrame = channels;
    destFormat.mSampleRate = sampleRate; 
    destFormat.mFramesPerPacket = 1;
    destFormat.mBytesPerFrame = destFormat.mBitsPerChannel * destFormat.mChannelsPerFrame / 8;
    destFormat.mBytesPerPacket = destFormat.mBytesPerFrame * destFormat.mFramesPerPacket;
    destFormat.mReserved = 0;
    
    return destFormat;
}

#pragma mark - <Render Call Back>
static OSStatus captureCallBack(void *inRefCon,
                                AudioUnitRenderActionFlags *ioActionFlags,
                                const AudioTimeStamp *inTimeStamp,
                                UInt32 inBusNumber,
                                UInt32 inNumberFrames,
                                AudioBufferList *ioData)
{
    
    CSMusicPlayerTool *tool = (__bridge CSMusicPlayerTool *)(inRefCon);
    ExtAudioFileRef extAudioFile = tool.extAudioFile;
    
    if (*ioActionFlags == kAudioUnitRenderAction_OutputIsSilence) {
        return noErr;
    }
    
    OSStatus status = ExtAudioFileRead(extAudioFile, &inNumberFrames, ioData);
    
    if (inNumberFrames == 0) {
        ExtAudioFileSeek(extAudioFile, 0);
    }
    
    if (status == noErr) {
        //        [AudioWriteToFile writeToFileWithData:bufferList.mBuffers[0].mData length:bufferList.mBuffers[0].mDataByteSize];
    }
    else {
        [tool errorWithOSStatus:status whereCall:@"AudioUnitRender"];
        return status;
    }
    
    return noErr;
}

@end
