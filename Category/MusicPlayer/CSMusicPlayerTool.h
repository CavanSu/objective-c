//
//  CSMusicPlayerTool.h
//
//  Created by CavanSu on 17/8/1.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface CSMusicPlayerTool : NSObject

#pragma mark - <AVAudioPlayer>
+ (void)audioPlayerPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop;
+ (void)audioPlayerPauseMusicWithMusicName:(NSString *)musicName;
+ (void)audioPlayerStopMusicWithMusicName:(NSString *)musicName;

#pragma mark - <AVPlayer>
+ (void)playerPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop;
+ (void)playerPauseMusic;
+ (void)playerStopMusic;

#pragma mark - <OutputUnit>
+ (void)outputUnitPlayMusicWithMusicName:(NSString *)musicName isLoop:(BOOL)isLoop;
+ (void)outputUnitPauseMusic;
+ (void)outputUnitStopMusic;

@end
