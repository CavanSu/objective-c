//
//  CSSoundEffect.h
//
//  Created by CavanSu on 17/8/1.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSSoundEffect : NSObject

// Play soundName
+ (void)playSoundWithSoundName:(NSString *)soundName;

@end
