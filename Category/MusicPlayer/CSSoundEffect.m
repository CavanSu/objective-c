//
//  CSSoundEffect.m
//
//  Created by CavanSu on 17/8/1.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import "CSSoundEffect.h"
#import <AVFoundation/AVFoundation.h>

static NSMutableDictionary *soundsDic;

@implementation CSSoundEffect

+ (void)initialize {
    soundsDic = [NSMutableDictionary dictionary];
}

+ (void)playSoundWithSoundName:(NSString *)soundName {
    // 1. 定义
    SystemSoundID soundID = 0;
    
    // 2. 从soundsDic 里取 soundID， 有则直接播放， 没有则创建，存入字典后播放
    soundID = [soundsDic[soundName] unsignedIntValue];
    
    BOOL wasStored = soundID == 0 ? NO : YES;
    
    // 3. 字典里不存在
    if (!wasStored) {
        CFURLRef urlRef = (__bridge CFURLRef)([[NSBundle mainBundle] URLForResource:soundName withExtension:nil]);
        
        // 加载音效文件，得到对应的音效ID
        AudioServicesCreateSystemSoundID(urlRef, &soundID);
        
        // 将soundID 存入字典
        [soundsDic setObject:@(soundID) forKey:soundName];
        
        CFRelease(urlRef);
    }
    
    // 4. 播放音效
//    AudioServicesPlaySystemSound(soundID);
    
    // 5. 播放音效且带振动
    AudioServicesPlayAlertSound(soundID);
}

@end
