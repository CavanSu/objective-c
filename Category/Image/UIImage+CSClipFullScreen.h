//
//  UIImage+CSClipFullScreen.h
//  截屏
//
//  Created by CavanSu on 17/3/21.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CSClipFullScreen)
+ (UIImage *)imageWithCaputureView:(UIView *)view;
@end
