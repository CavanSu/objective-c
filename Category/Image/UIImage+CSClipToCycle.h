//
//  UIImage+CSClipToCycle.h
//  图片裁剪
//
//  Created by CavanSu on 17/3/21.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CSClipToCycle)
+ (UIImage *)imageWithClipImage:(UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color;
@end
