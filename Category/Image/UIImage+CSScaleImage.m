//
//  UIImage+CSScaleImage.m
//  图片裁剪
//
//  Created by CavanSu on 17/7/6.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "UIImage+CSScaleImage.h"

@implementation UIImage (CSScaleImage)

+ (UIImage *)imageWithScaleImage:(UIImage *)image scale:(CGFloat)scale {
    // 1.缩放后的尺寸
    CGSize finalSize = CGSizeMake(image.size.width * scale, image.size.height * scale);
    
    // 2.开启上下文
    UIGraphicsBeginImageContextWithOptions(finalSize, NO, 0);
    
    // 3.绘图
    [image drawInRect:CGRectMake(0, 0, finalSize.width, finalSize.height)]; // 在 上下文 的哪个点开始绘制， 以及图片要绘制多大
    
    // 4.得到缩放后的图片
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 5.关闭上下文
    UIGraphicsEndImageContext();

    return finalImage;
}

@end
