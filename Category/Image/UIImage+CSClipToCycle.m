//
//  UIImage+CSClipToCycle.m
//  图片裁剪
//
//  Created by CavanSu on 17/3/21.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import "UIImage+CSClipToCycle.h"

@implementation UIImage (CSClipToCycle)

+ (UIImage *)imageWithClipImage:(UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color {
    // 图片的宽度和高度
    CGFloat imageWH = image.size.width >= image.size.height ? image.size.height : image.size.width;
    
    // 设置圆环的宽度
    CGFloat border = borderWidth;
    
    // 圆形的宽度和高度
    CGFloat ovalWH = imageWH + 2 * border;
    
    // 1.开启上下文
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(ovalWH, ovalWH), NO, 0);
    
    // 2.画大圆
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, ovalWH, ovalWH)];
    
    [color set];
    
    [path fill];
    
    // 3.设置裁剪区域
    UIBezierPath *clipPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(border, border, imageWH, imageWH)];
    [clipPath addClip];
    
    // 4.绘制图片
    
    /* 计算
       
     以图片的最中心的点为圆绘制出来
     
    */
    
    CGFloat startX = image.size.width <= imageWH ? ((image.size.width - imageWH) / 2) : ((imageWH - image.size.width) / 2);
    
    CGFloat startY = image.size.height <= imageWH ? ((image.size.height - imageWH) / 2) : ((imageWH - image.size.height) / 2);
    
    [image drawAtPoint:CGPointMake(startX, startY)]; // point 指 图片左上角的点，开始绘制在 上下文的哪个点上
    
    // 5.获取图片
    UIImage *clipImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 6.关闭上下文
    UIGraphicsEndImageContext();
    
    return clipImage;
}

@end
