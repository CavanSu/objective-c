//
//  UIImage+CSResizeImage.h
//
//  Created by CavanSu on 24/02/2017.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CSResizeImage)

/**
 *  传入图片名称,返回拉伸好的图片
 */
+ (UIImage *)resizeImage:(NSString *)imageName;
@end
