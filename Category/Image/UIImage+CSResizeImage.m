//
//  UIImage+CSResizeImage.m
//
//  Created by CavanSu on 24/02/2017.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import "UIImage+CSResizeImage.h"

@implementation UIImage (CSResizeImage)

/**
 *  传入图片名称,返回拉伸好的图片
 */
+ (UIImage *)resizeImage:(NSString *)imageName
{
    return [[UIImage imageNamed:imageName] resizeImage];
}

- (UIImage *)resizeImage
{
    CGFloat width = self.size.width * 0.5;
    CGFloat height = self.size.height * 0.5;
    return [self stretchableImageWithLeftCapWidth:width topCapHeight:height];
}


@end
