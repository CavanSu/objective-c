//
//  UIImage+CSScaleImage.h
//  图片裁剪
//
//  Created by CavanSu on 17/7/6.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CSScaleImage)
+ (UIImage *)imageWithScaleImage:(UIImage *)image scale:(CGFloat)scale;
@end
