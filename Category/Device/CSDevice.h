//
//  CSDevice.h
//
//  Created by CavanSu on 17/6/16.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSDevice : NSObject
+ (NSString *)getCurrentDevice;
+ (NSString *)getCurrentDeviceName;
@end
