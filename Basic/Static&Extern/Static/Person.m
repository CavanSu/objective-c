//
//  Person.m
//  Static
//
//  Created by CavanSu on 17/7/5.
//  Copyright (c) 2015年 CavanSu. All rights reserved.
//

#import "Person.h"

@implementation Person

static int age = 25;

+ (void)test {
    extern int age;
    // 优先使用 static描述的全局变量，而不是其他文件的全局变量
    NSLog(@"Person age: %d", age);
}

static void test2() {
    NSLog(@"static func");
}

@end
