//
//  main.m
//  Static
//
//  Created by CavanSu on 17/7/5.
//  Copyright (c) 2015年 CavanSu. All rights reserved.
//

/**
 static的作用：
 1.修饰局部变量
 * 让局部变量只初始化一次
 * 局部变量在程序中只有一份内存
 * 并不会改变局部变量的作用域，仅仅是改变了局部变量的生命周期（只到程序结束，这个局部变量才会销毁）
 
 2.修饰全局变量
 * 全局变量的作用域仅限于当前文件
 */

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Person.h"
#import "Dog.h"

void test() {
    for (int i = 0; i < 3; i++) {
        static int a = 0;
        a++;
        NSLog(@"a = %d", a);
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 1.Static 对局部变量的描述
        test();
        
        // 2.extern int age 不是定义的意思，而是引用 Car 文件中的age
        extern int age;
        NSLog(@"age is %d", age);
        
        // 3.比较 static 与 extern的优先级
        [Person test];
        
        // 4.extern 设置常量的用法
        NSLog(@"dogName: %@", dogName);
    }
    return 0;
}
