//
//  Dog.h
//  Static
//
//  Created by CavanSu on 2018/7/31.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

// 这里使用到extern c关键字，表示这个变量已经声明，只是引用。const关键字表示变量是常量，不可修改。
extern NSString *const dogName;

@interface Dog : NSObject

@end
