//
//  Dog.m
//  Static
//
//  Created by CavanSu on 2018/7/31.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import "Dog.h"

// 给头文件中的dogName赋值
NSString *const dogName = @"柴柴";

/*
 使用这种方式，比通过宏预定义的优点是，可以对常量进行指针比较操作，这是#define做不到的.
 
 例如：
 [dogAName isEqualToString:dogName];
 */


@implementation Dog

@end
