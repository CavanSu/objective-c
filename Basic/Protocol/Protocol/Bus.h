//
//  Bus.h
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Driving.h"

@interface Bus : NSObject <Driving> //遵守了协议，就要实现协议中的必选方法

@end
