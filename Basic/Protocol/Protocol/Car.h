//
//  Car.h
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Driving.h"

@interface Car : NSObject <Driving>

@end
