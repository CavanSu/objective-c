//
//  Ship.h
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Driving.h"

@interface Ship : NSObject <Driving>

@end
