//
//  Driving.h
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Driving <NSObject>

@required
- (void)run;
- (void)stop;

@optional
- (void)light;
- (void)dark;

@end
