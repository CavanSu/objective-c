//
//  Car.m
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import "Car.h"

@implementation Car

-(void)run {
    NSLog(@"Car run");
}

-(void)stop {
    NSLog(@"Car stop");
}
@end
