//
//  Bus.m
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import "Bus.h"

@implementation Bus
-(void)run {
    NSLog(@"Bus run");
}

-(void)stop {
    NSLog(@"Bus stop");
}
@end
