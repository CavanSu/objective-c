//
//  main.m
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Bus.h"

int main(int argc, const char * argv[]) {
    Car *car1 = [[Car alloc] init];
    [car1 run];
    [car1 stop];
    
    Bus *bus1 = [[Bus alloc] init];
    [bus1 run];
    [bus1 stop];
    
    return 0;
}
