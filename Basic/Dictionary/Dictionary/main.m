//
//  main.m
//  Dictionary
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
void ummutableDictionary(void);
void mutableDictionary(void);

int main(int argc, const char * argv[]) {
//    ummutableDictionary();
    mutableDictionary();
    
    return 0;
}

void ummutableDictionary() {
    NSDictionary *dicScores = [NSDictionary dictionaryWithObjectsAndKeys:@80, @"english", @90, @"maths", @100, @"computer", nil];
    NSNumber *englishScore = [dicScores objectForKey:@"english"];
    NSLog(@"English Socre: %@", englishScore);
    
    NSArray *arr = [dicScores allKeys];
    NSArray *arr2 = [dicScores allValues];
    NSLog(@"arr: %@", arr);
    NSLog(@"arr value: %@", arr2);
    
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"%@",[dicScores objectForKey:[arr objectAtIndex:i]]);
    }
    
    for (id key in dicScores) {
        NSLog(@"%@",[dicScores objectForKey:key]);
    }
    
    NSDictionary *dic  = @{@"k1":@"v1",@"k2":@"v2",@"k3":@"v3"};
    NSLog(@"%@",dic);
}

void mutableDictionary() {
    NSMutableDictionary *mutDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@80,@"english",@90,@"maths",@100,@"computer", nil];
    NSLog(@"%@",mutDic);
    
    // add object
    [mutDic setObject:@"v2" forKey:@"k1"];
    NSLog(@"%@",mutDic);
    
    [mutDic removeObjectForKey:@"k1"];
    mutDic[@"k1"] = @"test";
    NSLog(@"%@",mutDic);
}
