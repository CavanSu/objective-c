//
//  main.m
//  Const&Pointer
//
//  Created by CavanSu on 17/7/6.
//  Copyright (c) 2017年 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

void test(const int *p) {
    NSLog(@"Func test, p:%d, %p", *p, p);
    
    // const 修饰 参数 指针p 使指针p的值不能被修改
    // *p = 100;
}

void compare() {
    int p4 = 20;
    
    const int *p1;
    p1 = &p4;
    printf("*p1: %d \n", *p1);
    
    // const 修饰指针p1, 使指针p1的值不能被修改
    // *p1 = 20;
    
    int const *p2 = NULL;
    p2 = &p4;
    
    printf("*p2: %d \n", *p2);
    
    // const 修饰指针p2, 使指针p2的值不能被修改
    // *p2 = 10;
    
    // ------------------------
    //
    // const * : * 放在 const后面，能修改指针地址，不能修改指针所指向的值
    // * const : * 放在 const前面，能修改指针所指向的值，不能修改指针地址
    //
    // ------------------------

    int num = 10;
    test(&num);
    
    
    // 定义一个指针变量
    int * const p = NULL;
    
    // 定义2个int类型的变量
    int a = 10;
    int b = 30;
    
    // p指向a
    // p = &a;
    *p = 20;
    
    // p指向b
    // p = &b;
    *p = 40;
    
    NSLog(@"p: %d %d", a, b);
}

void review() {
    // 定义一个指针变量
    int *p = NULL;
    
    // 定义2个int类型的变量
    int a = 10;
    int b = 30;
    
    // p指向a
    p = &a;
    *p = 20;
    
    // p指向b
    p = &b;
    *p = 40;
    
    NSLog(@"%d %d", a, b);
}


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        review();
        compare();
    }
    
    return 0;
}
