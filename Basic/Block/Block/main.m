//
//  main.m
//  Block
//
//  Created by CavanSu on 06/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

int globalValue = 10;
static int staVaule = 20;

void blockWithoutParameter(void);
void blockWithParameter(void);
void blockWithRetain(void);


/*
 Conclution:
 
 - block 中对局部变量进行值 拷贝
 - block中修改局部变量的值， 在局部变量定义前加上 __block
 
 - block 对全局变量与静态变量进行引用
 - block 对对象进行引用的同时会使引用计数加1，在此容易造成循环引用
 
 */


int main(int argc, const char * argv[]) {
    
//    blockWithoutParameter();
//    blockWithParameter();
//    blockWithRetain();
    
    return 0;
}

void blockWithoutParameter() {
    // block的定义
    // void(^)() 像是一个数据类型，就像int, float.ect.
    // demoBlock 像是一个变量名，就像定义 int a 里的 a
    // ^(){} 像是具体的值
    void(^demoBlock)(void) = ^() {
        int a = 10, b = 20;
        int c = a + b;
        NSLog(@"c :%d", c);
    };
    
    // block的调用
    demoBlock();
}

void blockWithParameter() {
    // typedef: 将void(^)(int) 取别名为 subBlock
    typedef void(^subBlock)(int);
    
    subBlock sub = ^(int a) {
        int b = 10;
        NSLog(@"subBlock: %d", a + b);
    };
    
    sub(5);

    
    // 对局部变量进行值拷贝
    int test =  44;
    void(^block)(void) = ^(){
        NSLog(@"test: %d", test);
    };
    test = 55;
    block();
    
    
    // 如果在block中改变外部定义的局部变量值,必须加上关键字 __block
    __block int t = 10;
    
    int(^sumBlock)(int , int) = ^(int a, int b){
        t = a + b;
        return t;
    };
    
    int result = sumBlock(1, 2);
    NSLog(@"result :%d", result);
    NSLog(@"t :%d", t);
}

void blockWithRetain() {
    // 1. 全局变量，block内部能一直引用变量
    void (^block)(void) = ^{
        NSLog(@"globalValue : %d", globalValue);
    };
    
    globalValue = 20;
    block();
    
    // 2. 全局变量， 可以在Block内部直接改变
    void (^block1)(void) = ^{
        globalValue = 15;
        NSLog(@"globalValue : %d", globalValue);
    };
    
    block1();
    
    // 3. 静态变量，与全局变量一样
    void (^block2)(void) = ^{
        staVaule = 22;
        NSLog(@"staVaule : %d", staVaule);
    };
    
    block2();
}

