//
//  NSObject+Print.h
//  Category
//
//  Created by CavanSu on 18/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Print)
@property (nonatomic, copy) NSString *csCate;
- (void)print;
@end
