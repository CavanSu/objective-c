//
//  NSObject+Print.m
//  Category
//
//  Created by CavanSu on 18/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import "NSObject+Print.h"

@implementation NSObject (Print)
- (void)print {
    NSLog(@"This is a category");
}

- (void)setCsCate:(NSString *)csCate {
    NSLog(@"setCsCate");
}

- (NSString *)csCate {
    return @"csCate";
}
@end
