//
//  main.m
//  Category
//
//  Created by CavanSu on 18/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Print.h"

int main(int argc, const char * argv[]) {
    
    NSObject *obj = [[NSObject alloc] init];
    obj.csCate = @"123";
    NSLog(@"%@", [obj csCate]);
    
    [obj print];
    
    NSUInteger uid = 1222;
    
    [NSString stringWithFormat:@"%lu", uid];
    
    return 0;
}
