//
//  Exception.m
//  Exception
//
//  Created by CavanSu on 2019/2/28.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import "Exception.h"

@implementation MyException

@end

@implementation Exception
- (instancetype)init {
    if (self = [super init]) {
//        [self test];
//        [self test1];
//        [self uncaughtException];
        [self cUncaughtException];
    }
    return self;
}

- (void)test {
    @try {
        [self throwsMyEx];
    } @catch (MyException *exception) {
        NSLog(@"MyException, reason: %@", exception.reason);
    } @catch (NSException *exception) {
        NSLog(@"NSException, reason: %@", exception.reason);
    } @finally {
        NSLog(@"finally");
    }
}

- (void)test1 {
    @try {
        [self throwsNSEx];
    } @catch (MyException *exception) {
        NSLog(@"MyException, reason: %@", exception.reason);
    } @catch (NSException *exception) {
        NSLog(@"NSException, reason: %@", exception.reason);
    } @finally {
        NSLog(@"finally");
    }
}

static void uncaught_exception_handler (NSException *exception) {
    //可以取到 NSException 信息
    //...
    NSLog(@"uncaught: %@", exception.description);
    abort();
}

//信号处理函数
static void handleSignal( int sig ) {
    NSLog(@"c uncaught: %d", sig);
}

- (void)uncaughtException {
    //注册异常处理函数
    NSSetUncaughtExceptionHandler(&uncaught_exception_handler);
    //异常处理函数
}

- (void)cUncaughtException {
    //注册处理SIGSEGV信号
//    signal(SIGSEGV, handleSignal);
    
    
    for (int i = 1; i < 32; i++) {
        signal(i, handleSignal);
    }
    
    
//#define SIGHUP  1       /* hangup */
//#define SIGINT  2       /* interrupt */
//#define SIGQUIT 3       /* quit */
//#define SIGILL  4       /* illegal instruction (not reset when caught) */
//#define SIGTRAP 5       /* trace trap (not reset when caught) */
//#define SIGABRT 6       /* abort() */
//#if  (defined(_POSIX_C_SOURCE) && !defined(_DARWIN_C_SOURCE))
//#define SIGPOLL 7       /* pollable event ([XSR] generated, not supported) */
//#else   /* (!_POSIX_C_SOURCE || _DARWIN_C_SOURCE) */
//#define SIGIOT  SIGABRT /* compatibility */
//#define SIGEMT  7       /* EMT instruction */
//#endif  /* (!_POSIX_C_SOURCE || _DARWIN_C_SOURCE) */
//#define SIGFPE  8       /* floating point exception */
//#define SIGKILL 9       /* kill (cannot be caught or ignored) */
//#define SIGBUS  10      /* bus error */
//#define SIGSEGV 11      /* segmentation violation */
//#define SIGSYS  12      /* bad argument to system call */
//#define SIGPIPE 13      /* write on a pipe with no one to read it */
//#define SIGALRM 14      /* alarm clock */
//#define SIGTERM 15      /* software termination signal from kill */
//#define SIGURG  16      /* urgent condition on IO channel */
//#define SIGSTOP 17      /* sendable stop signal not from tty */
//#define SIGTSTP 18      /* stop signal from tty */
//#define SIGCONT 19      /* continue a stopped process */
//#define SIGCHLD 20      /* to parent on child stop or exit */
//#define SIGTTIN 21      /* to readers pgrp upon background tty read */
//#define SIGTTOU 22      /* like TTIN for output if (tp->t_local&LTOSTOP) */
//#if  (!defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE))
//#define SIGIO   23      /* input/output possible signal */
//#endif
//#define SIGXCPU 24      /* exceeded CPU time limit */
//#define SIGXFSZ 25      /* exceeded file size limit */
//#define SIGVTALRM 26    /* virtual time alarm */
//#define SIGPROF 27      /* profiling time alarm */
//#if  (!defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE))
//#define SIGWINCH 28     /* window size changes */
//#define SIGINFO 29      /* information request */
//#endif
//#define SIGUSR1 30      /* user defined signal 1 */
//#define SIGUSR2 31      /* user defined signal 2 */
    
    
    
    
}

- (void)throwsMyEx {
    @throw [MyException exceptionWithName:@"MyException" reason:@"hello" userInfo:nil];
}

- (void)throwsNSEx {
    @throw [NSException exceptionWithName:@"NSException" reason:@"world" userInfo:nil];
}

@end
