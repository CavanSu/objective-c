//
//  Exception.h
//  Exception
//
//  Created by CavanSu on 2019/2/28.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyException: NSException

@end

@interface Exception : NSObject
- (void)errorCall;
@end
