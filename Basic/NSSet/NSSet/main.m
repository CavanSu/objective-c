//
//  main.m
//  NSSet
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
   
    // 1. unmutable set
    NSSet *set1 = [NSSet setWithObjects:@100, @78, @90, nil];
    NSLog(@"%@",set1);
    
    // 2. mutable set
    // Set与数组类似，只是不是按顺序排列
    NSMutableSet *set2 = [NSMutableSet setWithObjects:@"test", @"10", @18, nil];

    // 可变集合可以增加,删除
    [set2 addObject:@"hello"];
    [set2 removeObject:@"10"];
    [set1 anyObject];
    
    if ([set1 isEqualToSet:set2]) {
        NSLog(@"set1 == set2");
    } else {
        NSLog(@"set1 != set2");
    }
    
    return 0;
}
