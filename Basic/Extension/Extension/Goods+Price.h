//
//  Goods+Price.h
//  Extension
//
//  Created by CavanSu on 18/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import "Goods.h"

@interface Goods ()
@property (nonatomic, copy) NSString *price;
@end
