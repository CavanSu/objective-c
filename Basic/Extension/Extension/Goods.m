//
//  Goods.m
//
//  Created by CavanSu on 16/7/21.
//  Copyright © 2016年 CavanSu. All rights reserved.
//

#import "Goods.h"
#import "Goods_Buy.h"
#import "Goods+Price.h"

@interface Goods ()
-(void)fun2;
@end

@implementation Goods
-(void)fun1 {
    NSLog(@"fun1");
    [self fun2];
}

-(void)fun2 {
    NSLog(@"fun2");
}

-(void)fun3 {
    NSLog(@"fun3");
    self.number = 2;
    NSLog(@"%f", self.number);
}

- (void)setPrice:(NSString *)price {
    _price = price;
    NSLog(@"price: %@", price);
}
@end
