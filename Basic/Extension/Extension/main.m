//
//  main.m
//  Extension
//
//  Created by CavanSu on 18/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Goods.h"
#import "Goods_Buy.h"
#import "Goods+Price.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Goods *goods = [[Goods alloc] init];
        goods.price = @"123 dollars";
        [goods fun1];
        [goods fun3];
    }
    return 0;
}
