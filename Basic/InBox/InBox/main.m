//
//  main.m
//  InBox
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 In Box:
 将数值类型(Int, Float, struct, ect.)转成对象
 
 Out Box:
 将对象转成数值类型(Int, Float, struct, ect.)
 
 */

int main(int argc, const char * argv[]) {
  
    // 1.In Box
    NSNumber *iValue = [NSNumber numberWithInt:12];
    NSNumber *fValue = [NSNumber numberWithFloat:12.5];
    NSArray *arr = @[iValue, fValue];
    NSLog(@"array: %@", arr);
    
    // 2.Out Box
    int a = [iValue intValue];
    NSLog(@"iValue: %d", a);
    float f = [fValue floatValue];
    NSLog(@"floatValue: %f", f);
    NSString *str = [iValue stringValue];
    NSLog(@"stringValue: %@", str);
    
    
    // Struct In Box
    NSRange range = {3,2};
    NSValue *vRang = [NSValue valueWithRange:range];
    NSLog(@"vRang: %@", vRang);
    
    
    struct WXYPiont{
        float x;
        float y;
    };
    
    struct WXYPiont p = {19.5, 7.6};
    
    // 自定义结构体的装箱: 参数1是结构体变量地址, 参数2放置类型
    NSValue *v1 = [NSValue value:&p withObjCType:@encode(struct WXYPiont)];
    //拆箱:将v1拆箱到p1
    struct WXYPiont p1;
    [v1 getValue:&p1];
    NSLog(@"v1, x: %f, y: %f", p1.x, p1.y);
    
    // 在这里也是装箱
    NSArray *array = @[@1, @2, @3];
    NSLog(@"array: %@", array);
    
    return 0;
}
