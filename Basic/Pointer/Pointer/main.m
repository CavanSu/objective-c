//
//  main.m

#import <Foundation/Foundation.h>

void case1() {
    int numbers[4] = {10, 20, 30, 40};
    int intSize = sizeof(int);
    NSLog(@"intSize: %d", intSize);
    
    // numbers 就是指向数组首元素的指针， 与加了取地址符 &numbers打印结果一样
    NSLog(@"numbers: %p", numbers);
    NSLog(@"&numbers: %p", &numbers);
    
    int index = 0;
    switch (index) {
        case 0: {
            // 所以*a, *b打印结果一样
            int *a = (int *)(&numbers);
            NSLog(@"*a: %d, %p", *a, a);
            
            int *b = numbers;
            NSLog(@"*b: %d, %p", *b, b);
            
            // 直接通过下标值取得元素
            NSLog(@"list index2: %d", a[2]);
            
            // b + 1 时，1表示b所指向的地址的数据类型长度，例如这里是 int型，所以直接加上 int的长度4
            b = b + 1;
            NSLog(@"*b: %d, %p", *b, b);
        }
            break;
            
        case 1: {
            // &numbers + 1, 在内存地址上直接加上 16, 因为内存地址是以16进制记录地址
            int *c = (int *)(&numbers + 1);
            NSLog(@"*c: %d, %p", *c, c);
            
            // c - 1 时，1表示c所指向的地址的数据类型长度，例如这里是 int型，所以直接加上 int的长度4
            NSLog(@"*(c - 1): %d, %p", *(c - 1), (c - 1));
        }
            break;
            
        case 2: {
            // numbers + 1 时，1表示c所指向的地址的数据类型长度，例如这里是 int型，所以直接加上 int的长度4
            int *d = (int *)(numbers + 1);
            NSLog(@"*d: %d, %p", *d, d);
            
        }
            break;
        default:
            break;
    }
}

void case2() {
    int numbers[2][2] = {
        {10, 20}, // numbers[0]
        {11, 22} // numbers[1]
    };
    
    // numbers[0], 是指向 numbers[0][0]的地址
    NSLog(@"numbers[0]: %d, %p, %p", numbers[0][0], &numbers[0][0], numbers[0]);
    
    // numbers[0], 是指向 numbers[0][0]的地址
    NSLog(@"numbers[1]: %d, %p, %p", numbers[1][0], &numbers[1][0], numbers[1]);
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int index = 0;
        switch (index) {
            case 0:
                case1();
                break;
            case 1:
                case2();
                break;
            default:
                break;
        }
    }
    return 0;
}



