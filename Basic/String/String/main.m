//
//  main.m
//  04- String
//
//  Created by CavanSu on 31/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

void immutableString(void);
void initOfString(void);
void capitalAndlowercaseOfString(void);
void orderCompareOfString(void);
void subString(void);
void writeAndRead(void);
void translateCstringWithOCstring(void);
void mutableString(void);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
#pragma mark - <不可变字符串>
        // 1.比较字符串
//        immutableString();
        
        // 2.初始化方法
//        initOfString();
        
        // 3.大小写
//        capitalAndlowercaseOfString();
        
        // 4.截取字符串
//        subString();
        
        // 5.读写
//        writeAndRead();
        
        // 6.
//        translateCstringWithOCstring();
        
#pragma mark - <可变字符串>
        mutableString();
    }
    return 0;
}

void immutableString() {
    //1.比较字符串
    NSString *str1 = @"123";
    NSString *str2 = @"123";
    NSLog(@"str1:%p  str2:%p",str1,str2);
    if (str1 == str2) { // 比较两个字符串的地址
        NSLog(@"str1 == str2");
    }
    else {
        NSLog(@"str1 != str2");
    }
    
    if ([str1 isEqualToString:str2]) { // 比较两个字符串的值
        NSLog(@"str1 == str2");
    }
    else {
        NSLog(@"str1 != str2");
    }
    // Conclution: OC字符串存于 字符区，所以地址相同，值相同
}

void initOfString() {
    //2.初始化方法  "my name is " + "tom"
    NSString *tom = @"tom";
    //对象方法
    NSString *info = [[NSString alloc] initWithFormat:@"my name is: %@", tom];
    NSLog(@"%@",info);
    //类方法
    NSString *info2 = [NSString stringWithFormat:@"my name is: %@", tom];
    NSLog(@"%@",info2);
}

void capitalAndlowercaseOfString() {
    NSString *string = @"ABCdef";
    // 小写
    NSLog(@"%@", [string lowercaseString]);
    // 大写
    NSLog(@"%@", [string uppercaseString]);
    // 仅开头大写
    NSLog(@"%@", [string capitalizedString]);
    // 字符串长度
    NSLog(@"%lu", (unsigned long)[string length]);
}

void orderCompareOfString() {
    NSString *str1 = @"abc";
    NSString *str2 = @"def";
    
    //4.字符串的比较
    NSComparisonResult result = [str1 compare:str2];
    
    if (result == NSOrderedSame) {
        NSLog(@"str1==str2");
    } else if (result == NSOrderedAscending) {
        NSLog(@"str1 < str2");
    } else {
        NSLog(@"str1 > str2");
    }
}

void subString() {
    NSLog(@"%@",[@"abcdefh" substringFromIndex:3]);
    NSLog(@"%@",[@"abcdefh" substringToIndex:5]);
    NSLog(@"%@",[@"abcdefh" substringWithRange:NSMakeRange(2, 3)]);
}

void writeAndRead() {
    NSString *strHello = @"hello everyone!";
    [strHello writeToFile:@"/Users/suruoxi/Desktop/test" atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSString *strHello2 = [[NSString alloc] initWithContentsOfFile:@"/Users/suruoxi/Desktop/test"  encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@", strHello2);
}

void translateCstringWithOCstring() {
    char *ch = "test case";
    NSString *chOC = [[NSString alloc] initWithCString:ch encoding:NSUTF8StringEncoding];
    NSLog(@"OC String: %@", chOC);
    char *ch2 = (char *)[chOC UTF8String];
    NSLog(@"C String: %s", ch2);
}

void mutableString() {
//    NSMutableString *mutstr = [NSMutableString stringWithCapacity:20];
    NSMutableString *mutstr2 = [[NSMutableString alloc] init];
    NSLog(@"mutstr2: %@", mutstr2);
    // 可变字符串和不可变字符串之间的关系:继承的关系 -> 父类:不可变字符串
    [mutstr2 setString:@"test case"]; //setString进行赋值
    NSLog(@"mutstr2: %@", mutstr2);
    NSString *tom =  @"tom";
    NSLog(@"mutstr2: %@", mutstr2);
    [mutstr2 appendString:@"12345"];
    NSLog(@"mutstr2: %@", mutstr2);
    [mutstr2 appendFormat:@"%@",tom];
    NSLog(@"mutstr2: %@", mutstr2);
    //可变字符串替换
    [mutstr2 replaceCharactersInRange:NSMakeRange(3, 4) withString:@"new string"];
    NSLog(@"mutstr2: %@", mutstr2);
    //可变字符串插入
    [mutstr2 insertString:@"hello" atIndex:2];
    NSLog(@"mutstr2: %@", mutstr2);
    //可变字符串删除
    [mutstr2 deleteCharactersInRange:NSMakeRange(3, 3)];
    NSLog(@"mutstr2: %@", mutstr2);
}
