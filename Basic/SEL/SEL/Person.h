//
//  Person.h
//  SEL
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property(nonatomic,assign) int age;
@property(nonatomic,copy) NSString *name;
- (void)speak;
- (void)speakWithPerson:(Person*)person at:(NSString*)someWhere;
@end
