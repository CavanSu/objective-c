//
//  Person.m
//  SEL
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import "Person.h"

@implementation Person

- (void)speak {
    NSLog(@"speak");
}

- (void)speakWithPerson:(Person*)person at:(NSString*)someWhere {
    NSLog(@"%@ is speaking with %@ at %@", self.name, person.name, someWhere);
}

@end
