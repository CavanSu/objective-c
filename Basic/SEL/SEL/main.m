//
//  main.m
//  SEL
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
   
    Person *tom = [[Person alloc] init];
    tom.name = @"tom";
    Person *jack = [[Person alloc] init];
    jack.name = @"jack";
    [tom speak];
    [tom speakWithPerson:jack at:@"classRoom"];
    
    SEL selSpeak = @selector(speak);
    
    if ([tom respondsToSelector:selSpeak]) {
        [tom performSelector:@selector(speak)];
    }
    
    SEL selSpeakAt = @selector(speakWithPerson:at:);
    
    if ([tom respondsToSelector:selSpeakAt]) {
        [tom performSelector:@selector(speakWithPerson:at:) withObject:jack withObject:@"classRoom"];
    }
    
    return 0;
}
