//
//  Person.h
//  Person
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
 // copy, 浅拷贝
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSMutableString *title;
// 重写 gender 的 set, 使用 mutableCopy
@property (nonatomic, copy) NSMutableString *gender;
@end

NS_ASSUME_NONNULL_END
