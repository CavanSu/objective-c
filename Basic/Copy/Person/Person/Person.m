//
//  Person.m
//  Person
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import "Person.h"

@implementation Person
- (void)setGender:(NSMutableString *)gender {
    _gender = [gender mutableCopy];
}
@end
