//
//  main.m
//  Person
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

void test() {
    NSMutableString *mStr = [NSMutableString stringWithString:@"Boss"];
    Person *p = [[Person alloc] init];
    
    // NSMutableString copy 到 NSMutableString， 得到的是 NSString
    p.title = mStr;
    
    // 打印地址, 得知 p.title 是生成了一个新的 NSString 对象
    NSLog(@"p.title add: %p, mStr: %p", p.title, mStr);
    
    // NSString 没有 setString 方法， 所以这里就挂了
    [p.title setString:@"Cav"];
}

void test2() {
    NSMutableString *mStr = [NSMutableString stringWithString:@"Female"];
    Person *p = [[Person alloc] init];
    
    // NSMutableString mutableCopy 到 NSMutableString， 得到的还是 NSMutableString
    p.gender = mStr;
    
    // 打印地址, 得知 p.title 是生成了一个新的 NSMutableString 对象
    NSLog(@"gender add: %p, mStr: %p", p.gender, mStr);
    
    // NSString 没有 setString 方法， 所以这里就挂了
    [p.gender setString:@"Male"];
    
    NSLog(@"gender: %@", p.gender);
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        test2();
    }
    return 0;
}
