//
//  main.m
//  Copy
//

#import <Foundation/Foundation.h>
#import "Person.h"

void array() {
    NSArray *array = @[@"1", @"2"];
    
    NSArray *array1 = [array copy];
    
    NSMutableArray *array2 = [array mutableCopy];
    [array2 addObject:@"3"];
    
    NSLog(@"%@ %@", array, array2);
    NSLog(@"%p, %p", &array, &array2);
}

void string() {
    NSString *string = @"12";
    
    NSString *str1 = [string copy]; // 没有产生新对象
    NSMutableString *str2 = [string mutableCopy]; // 产生新对象
    
    NSLog(@"%p %p %p", string, str1, str2);
    NSLog(@"%@ %@ %@", string, str1, str2);
    
    string = @"312"; // string 指向其他内存区域
    NSLog(@"after string value changed");
    NSLog(@"%p %p %p", string, str1, str2);
    NSLog(@"%@ %@ %@", string, str1, str2);
}

void mutableString() {
    NSMutableString *string = [NSMutableString string];
    [string appendString:@"1"];
    [string appendString:@"2"];
    
    NSString *str1 = [string copy]; // 产生新对象
    NSMutableString *str2 = [string mutableCopy]; // 产生新对象
    
    [str2 appendString:@"123"];
    
    [string appendString:@"345"];
    
    NSLog(@"%@ - %@", string, str1);
    NSLog(@"%p %p %p", string, [string copy], [string mutableCopy]);
    
}

void person() {
    Person *p = [[Person alloc] init];
    p.age = 10;
    p.money = 100.6;
    
    Person *p2 = [p copy];
    NSLog(@"%p, %p", &p, &p2);
}


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        string();
    }
    return 0;
}

