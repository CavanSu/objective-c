//
//  main.m
//  Array
//
//  Created by CavanSu on 05/04/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

#import <Foundation/Foundation.h>
void ummutableArray(void);
void mutableArray(void);
void sort(void);

int main(int argc, const char * argv[]) {
   
//    ummutableArray();
//    mutableArray();
    sort();
    
    return 0;
}

void ummutableArray() {
    // 1.初始化对象方法
    NSArray *arrFruit = [[NSArray alloc] initWithObjects:@"apple",@"pear",@"banana",@"watermelon" ,nil];
    NSLog(@"arrFruit:%@",arrFruit);
    
    // 2.类方法
    NSArray *arrFruit2 = [NSArray arrayWithObjects:@"张三",@"apple",@"pear",@"banana",@"watermelon" , nil];
    NSLog(@"arrFruit2:%@",arrFruit2);
    NSLog(@"arrFruit2 count:%ld", [arrFruit2 count]);
    
    NSString *strObj = [arrFruit objectAtIndex:2];
    NSLog(@"%@",strObj);
    
    // 数组的初始化方法
    NSArray *arrFruit3 = @[@"apple", @"pear", @"banana", @"watermelon", @10, @18.5];
    NSLog(@"%@",arrFruit3);
}

void mutableArray() {
    NSMutableArray *mutarr = [NSMutableArray array];
    [mutarr addObject:@"OC"];
    [mutarr addObject:@"java"];
    [mutarr addObject:@"C#"];
    [mutarr addObject:@"PYTHON"];
    [mutarr addObject:@"swift"];
    [mutarr addObject:@"VB"];
    [mutarr addObject:@"C++"];
    [mutarr addObject:@"PHP"];
    
    [mutarr removeObject:@"C#"];
    NSLog(@"mutarr:%@", mutarr);
    
    [mutarr replaceObjectAtIndex:2 withObject:@"JAVASCRIPT"];
    NSLog(@"mutarr:%@", mutarr);
    
    [mutarr insertObject:@"Ada" atIndex:3];
    NSLog(@"mutarr:%@", mutarr);
    
    [mutarr removeObjectAtIndex:1];
    NSLog(@"mutarr:%@", mutarr);
    
    [mutarr removeAllObjects];
    NSLog(@"mutarr:%@", mutarr);
    
    [mutarr removeLastObject];
    NSLog(@"mutarr:%@", mutarr);
}

void sort() {
    NSArray *array = @[@(1), @(10), @(5), @(3)];
    
    NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSNumber *  _Nonnull obj1,
                                                                                 NSNumber *  _Nonnull obj2) {
        // 升序
//        if (obj1.intValue > obj2.intValue) {
//            return NSOrderedDescending;
//        } else {
//            return NSOrderedAscending;
//        }
        
        // 降序
        if (obj1.intValue < obj2.intValue) {
            return NSOrderedDescending;
        } else {
            return NSOrderedAscending;
        }
    }];
    
    NSLog(@"sortedArray: %@", sortedArray);
}
